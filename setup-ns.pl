#!/usr/bin/perl
# (C) 2022-2024 <adrian.ulrich@blinkenlights.ch>
# Licensed under the MIT license
#
use strict;
use constant NS_MAXLEN => 15 - length("-nrwg0");
use Getopt::Std;

my $opts = {};

getopts('tdrn:4:6:w:W:', $opts) or HELP_MESSAGE();

my $TRACE  = $opts->{t};
my $NSNAME = $opts->{n} or HELP_MESSAGE();
my $WG_ROUTE_IF  = "$NSNAME-wg0";
my $WG_NOROUTE_IF = "$NSNAME-nrwg0";

my $DO_ROUTE = $opts->{r};

my $IPV4 = (int($opts->{4}) && "10.97.".int($opts->{4}));
my $IPV6 = (int($opts->{6}) && "fd00:97::".int($opts->{6}));

my $WG_ROUTE_CONF = $opts->{w};
my $WG_NOROUTE_CONF = $opts->{W};

if ($DO_ROUTE && $WG_ROUTE_CONF) {
    die "Cant create a default route (-r) while also creating a wireguard interface (-w) which would get the default route\n";
}
if (length($NSNAME) > NS_MAXLEN) {
    die "-n can be at most ".NS_MAXLEN." chars\n";
}

remove_old($NSNAME, $WG_ROUTE_IF);
remove_old($NSNAME, $WG_NOROUTE_IF);
exit(0) if $opts->{d};

setup_ns($NSNAME);

setup_ipv4($NSNAME, $IPV4.".1", $IPV4.".2", $DO_ROUTE) if $IPV4;
setup_ipv6($NSNAME, $IPV6.":1", $IPV6.":2", $DO_ROUTE) if $IPV6;

# ensure lo is up after we configured everything:
sx("ip netns exec $NSNAME ip link set dev lo up");

setup_wg($NSNAME, $WG_ROUTE_CONF, $WG_ROUTE_IF, 1) if defined($WG_ROUTE_CONF);
setup_wg($NSNAME, $WG_NOROUTE_CONF, $WG_NOROUTE_IF, 0) if defined($WG_NOROUTE_CONF);

sub setup_ipv4() {
	my($ns, $out, $in, $route) = @_;
	my @devs = nsdevs($ns);

	sx("ip addr add $out/24 dev $devs[0]");
	sx("ip netns exec $ns ip addr add 127.0.0.1/8 dev lo");
	sx("ip netns exec $ns ip addr add $in/24 dev $devs[1]");
	if ($route) {
		sx("ip netns exec $ns ip route add default via $out");
	}
}

sub setup_ipv6() {
	my($ns, $out, $in, $route) = @_;
	my @devs = nsdevs($ns);

	sx("ip -6 addr add $out/112 dev $devs[0]");
	sx("ip netns exec $ns ip -6 addr add ::1/128 dev lo");
	sx("ip netns exec $ns ip -6 addr add $in/112 dev $devs[1]");
	if ($route) {
		sx("ip netns exec $ns ip -6 route add default via $out");
	}
}

sub setup_ns() {
	my($ns) = @_;

	my @devs = nsdevs($ns);
	sx("ip netns add $ns");
	sx("ip link add $devs[0] type veth peer name $devs[1] netns $ns");
	sx("ip link set dev $devs[0] up");
	sx("ip netns exec $ns ip link set dev $devs[1] up");
}

sub remove_old() {
	my($ns, @extra) = @_;

	foreach my $dev (nsdevs($ns), @extra) {
		sx("ip link set dev $dev down 2> /dev/null");
		sx("ip link delete $dev 2> /dev/null");
	}
	sx("ip netns delete $ns 2> /dev/null");
}

sub nsdevs() {
	my($ns) = @_;
	return ("${ns}-out", "${ns}-int");
}

sub setup_wg() {
    my($ns, $conf, $ifname, $add_default_route) = @_;
    sx("ip link add $ifname type wireguard");
    sx("ip link set $ifname netns $ns");

    open(CONF, "<", $conf) or die "could not read wireguard conf: $conf\n";
    while(<CONF>) {
        if ($_ =~ /^Address\s*=\s*(.+)$/) {
            print "GOT: $1\n";
            foreach my $addr (split(/\s*,\s*/,$1)) {
                sx("ip -n $ns addr add $addr dev $ifname");
            }
        }
    }
    close(CONF);
    sx("grep -Eiv '^(Address|MTU)' $conf | \
         ip netns exec $ns wg setconf $ifname /dev/stdin");
    sx("ip -n $ns link set up $ifname");
    if ($add_default_route) {
	sx("ip -4 -n $ns route add default dev $ifname");
	sx("ip -6 -n $ns route add default dev $ifname");
    }
}

sub sx() {
	my(@cmd) = @_;
	print "# executing @cmd\n" if $TRACE;
	return system(@cmd);
}

sub HELP_MESSAGE() {
die <<"EOF"
Usage: $0 -n name [-4 3] [-6 3] [-r] [-d] [-w /etc/wireguard/wgX.conf]

 -t      : Trace commands
 -r      : Setup route
 -d      : Just delete the namespace
 -n X    : Name of namespace
 -4 N    : Setup IPv4, using provided index
 -6 N    : Setup IPv6, using provided index
 -w conf : (also) setup a wireguard interface using the supplied conf.
 -W conf : same as -w, but do not set a default route

Example:
 # $0 -r -6 9
EOF
}
